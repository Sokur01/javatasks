package ru.sokur.module00.task14;

/*
Число в квадрате
*/

public class Solution {
    public static void main(String[] args) {
       sqr(5);
       System.out.println(sqr(5));
    }

    public static int sqr(int a) {
        return a * a;
    }
}
